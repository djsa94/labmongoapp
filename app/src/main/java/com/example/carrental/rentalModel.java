package com.example.carrental;

public class rentalModel {
    private String clientId;
    private String licensePlate;
    private int duration;
    private int cost;

    public rentalModel() {
    }

    public rentalModel(String clientId, String licensePlate, int duration, int cost) {
        this.clientId = clientId;
        this.licensePlate = licensePlate;
        this.duration = duration;
        this.cost = cost;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
