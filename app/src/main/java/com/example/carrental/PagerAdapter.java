package com.example.carrental;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FragmentSearchPlate tab1 = new FragmentSearchPlate();
                return tab1;
            case 1:
                FragmentSearchModel tab2 = new FragmentSearchModel();
                return tab2;
            case 2:
                FragmentSearchBrand tab3 = new FragmentSearchBrand();
                return tab3;
            case 3:
                FragmentSearchPriceRange tab4 = new FragmentSearchPriceRange();
                return tab4;
            case 4:
                FragmentBrandReport tab5 = new FragmentBrandReport();
                return tab5;
            case 5:
                FragmentCar tab6 = new FragmentCar();
                return  tab6;
            case 6:
                FragmentClient tab7= new FragmentClient();
                return tab7;
            case 7:
                FragmentRental tab8= new FragmentRental();
                return tab8;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}