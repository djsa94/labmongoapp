package com.example.carrental;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragmentRental extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rental, container, false) ;
        Button botonBuscar = (Button) view.findViewById(R.id.btnrentalDelete);
        Button botonCrear = (Button) view.findViewById(R.id.btnrentalCreate);
        Button botonUpdate = (Button) view.findViewById(R.id.btnrentalUpdate);
        final TextView txID = (TextView) view.findViewById(R.id.rentalCRUDclientID);
        final TextView txLicense = (TextView) view.findViewById(R.id.rentalCRUDLicensePlate);
        final TextView txDuration = (TextView) view.findViewById(R.id.rentalCRUDduration);




        botonCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.createRental(getActivity(), getFragmentManager(), txID.getText().toString(), txLicense.getText().toString(), txDuration.getText().toString());
            }

        });

        botonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.updateRental( getActivity(), getFragmentManager(), txID.getText().toString(), txLicense.getText().toString(), txDuration.getText().toString()

                );
            }

        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.deleteRental( getActivity() ,getFragmentManager(), txID.getText().toString()

                );
            }

        });

        return view;


    }
}
