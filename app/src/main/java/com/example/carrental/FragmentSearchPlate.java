package com.example.carrental;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragmentSearchPlate extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_plate_layout, container, false) ;
        Button botonBuscar = (Button) view.findViewById(R.id.btnSearchLicensePlate);
        final TextView tx = (TextView) view.findViewById(R.id.ETlicencePlateText);

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.getCar(getFragmentManager(), tx.getText().toString());
            }

        });
        return view;

    }
}
