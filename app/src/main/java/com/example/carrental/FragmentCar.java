package com.example.carrental;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragmentCar extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car, container, false) ;
        Button botonBuscar = (Button) view.findViewById(R.id.btncarCRUDSearch);
        Button botonCrear = (Button) view.findViewById(R.id.btncarCRUDcreate);
        Button botonUpdate = (Button) view.findViewById(R.id.btncarCRUDupdate);
        final TextView txLicensePlate = (TextView) view.findViewById(R.id.carCRUDlicensePlate);
        final TextView txBrandId = (TextView) view.findViewById(R.id.carCRUDbrandId);
        final TextView txCapacity = (TextView) view.findViewById(R.id.carCRUDcapacity);
        final TextView txTransmition = (TextView) view.findViewById(R.id.carCRUDtransmition);
        final TextView txStyle = (TextView) view.findViewById(R.id.carCRUDstyle);
        final TextView txModel = (TextView) view.findViewById(R.id.carCRUDmodel);
        final TextView txColor = (TextView) view.findViewById(R.id.carCRUDColor);
        final TextView txCylinder = (TextView) view.findViewById(R.id.carCRUDcylinder);
        final TextView txfuelType = (TextView) view.findViewById(R.id.carCRUDfuelType);
        final TextView txYear = (TextView) view.findViewById(R.id.carCRUDyear);
        final TextView txExtras = (TextView) view.findViewById(R.id.carCRUDextras);
        final TextView txPassengers = (TextView) view.findViewById(R.id.carCRUDpassenger);
        final TextView txDailyCost = (TextView) view.findViewById(R.id.carCRUDdailyCost);
        final TextView txStatus = (TextView) view.findViewById(R.id.carCRUDstatus);


        botonCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.createCar( getActivity() ,getFragmentManager(), txLicensePlate.getText().toString(),
                        Integer.parseInt(txCapacity.getText().toString()),
                        Integer.parseInt(txBrandId.getText().toString()),
                        Integer.parseInt(txStyle.getText().toString()),
                        txModel.getText().toString(),
                        txColor.getText().toString(),
                        Integer.parseInt(txCylinder.getText().toString()),
                        txfuelType.getText().toString(),
                        txTransmition.getText().toString(),
                        Integer.parseInt(txYear.getText().toString()),
                        txExtras.getText().toString(),
                        Integer.parseInt(txPassengers.getText().toString()),
                        Integer.parseInt(txDailyCost.getText().toString()),
                        txStatus.getText().toString()

                        );
            }

        });

        botonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.updateCar( getActivity() ,getFragmentManager(), txLicensePlate.getText().toString(),
                        Integer.parseInt(txCapacity.getText().toString()),
                        Integer.parseInt(txBrandId.getText().toString()),
                        Integer.parseInt(txStyle.getText().toString()),
                        txModel.getText().toString(),
                        txColor.getText().toString(),
                        Integer.parseInt(txCylinder.getText().toString()),
                        txfuelType.getText().toString(),
                        txTransmition.getText().toString(),
                        Integer.parseInt(txYear.getText().toString()),
                        txExtras.getText().toString(),
                        Integer.parseInt(txPassengers.getText().toString()),
                        Integer.parseInt(txDailyCost.getText().toString()),
                        txStatus.getText().toString()

                );
            }

        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.deleteCar( getActivity() ,getFragmentManager(), txLicensePlate.getText().toString()

                );
            }

        });

        return view;


    }
}
