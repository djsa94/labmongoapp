package com.example.carrental;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragmentSearchPriceRange extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_price_layout, container, false) ;
        Button botonBuscar = (Button) view.findViewById(R.id.btnSearchPriceRange);
        final TextView txTop = (TextView) view.findViewById(R.id.ETpriceRangeTop);
        final TextView txBot = (TextView) view.findViewById(R.id.ETpriceRangeBot);
        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.getCarsByPrice(getFragmentManager(), txTop.getText().toString(), txBot.getText().toString());
            }

        });
        return view;
    }
}
