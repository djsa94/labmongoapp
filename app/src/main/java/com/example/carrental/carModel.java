package com.example.carrental;

public class carModel {
    private String licensePlate;
    private int capacity;
    private int brandId;
    private int styleId;
    private String carModel;
    private String carColor;
    private int cylinder;
    private String[] fuelType;
    private String[] transmitionType;
    private int year;
    private String[] extras;
    private int passengerCapacity;
    private int dailyCost;
    private String[] carStatus;

    public carModel(String licensePlate, int capacity, int brandId, int styleId, String carModel, String carColor, int cylinder, String[] fuelType, String[] transmitionType, int year, String[] extras, int passengerCapacity, int dailyCost, String[] carStatus) {
        this.licensePlate = licensePlate;
        this.capacity = capacity;
        this.brandId = brandId;
        this.styleId = styleId;
        this.carModel = carModel;
        this.carColor = carColor;
        this.cylinder = cylinder;
        this.fuelType = fuelType;
        this.transmitionType = transmitionType;
        this.year = year;
        this.extras = extras;
        this.passengerCapacity = passengerCapacity;
        this.dailyCost = dailyCost;
        this.carStatus = carStatus;
    }

    public carModel() {
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public int getCylinder() {
        return cylinder;
    }

    public void setCylinder(int cylinder) {
        this.cylinder = cylinder;
    }

    public String[] getFuelType() {
        return fuelType;
    }

    public void setFuelType(String[] fuelType) {
        this.fuelType = fuelType;
    }

    public String[] getTransmitionType() {
        return transmitionType;
    }

    public void setTransmitionType(String[] transmitionType) {
        this.transmitionType = transmitionType;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String[] getExtras() {
        return extras;
    }

    public void setExtras(String[] extras) {
        this.extras = extras;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }

    public int getDailyCost() {
        return dailyCost;
    }

    public void setDailyCost(int dailyCost) {
        this.dailyCost = dailyCost;
    }

    public String[] getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(String[] carStatus) {
        this.carStatus = carStatus;
    }

    public String carString(){
        String salida = "";
        salida += "License Plate: " +this.licensePlate + "\n" ;
        salida += "Car Capacity: " + this.capacity + "\n" ;
        salida += "Brand: " + this.brandId + "\n" ;
        salida += "Style: " + this.styleId + "\n" ;
        salida += "Model: " + this.carModel + "\n" ;
        salida += "Color: " + this.carColor + "\n" ;
        salida += "Cylinders: " + this.cylinder + "\n" ;
        salida += "Fuel: " + this.fuelType[0] + "\n" ;
        salida += "Transmition: " + this.transmitionType[0] + "\n" ;
        salida += "Year: " + this.year + "\n" ;
        salida += "Extras: " + this.extras.toString() + "\n" + "\n" ;
        salida += "Passengers: " + this.passengerCapacity+ "\n"  ;
        salida += "Daily Cost: " + this.dailyCost+ "\n"  ;
        salida += "Status: " + this.carStatus[0] + "\n" ;
        return salida;
    }
}


