package com.example.carrental;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HttpUtils {



        String url;
        RequestQueue queue;
        Gson gson;

        public HttpUtils(Context contexto) {
            // Instantiate the RequestQueue.
            queue = Volley.newRequestQueue(contexto);
            url = "https://happy-lionfish-71.localtunnel.me";
            gson = new Gson();
        }

        public void getCars(FragmentManager fragmentManager) {
            final FragmentManager fm = fragmentManager;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url +"/cars",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {// Display the first 500 characters of the response string.
                            Log.i("LLAMADA API", response);

                                Log.d("CREATION", response);
                                Gson gson = new Gson();
                                Bundle bundle = new Bundle();
                                bundle.putString("cars", response);
                                Fragment fragmentoNuevo = new listDisplay();
                                fragmentoNuevo.setArguments(bundle);
                                fm.beginTransaction().replace(R.id.main_layout, fragmentoNuevo).addToBackStack(null).commit();


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("LLAMADA API", error.getMessage());
                }
            });
            queue.add(stringRequest);

        }
    public void getCar(FragmentManager fragmentManager, String plate) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url + "/cars/" + plate ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Gson gson = new Gson();
                        Bundle bundle = new Bundle();
                        bundle.putString("cars", response);
                        Fragment fragmentoNuevo = new listDisplay();
                        fragmentoNuevo.setArguments(bundle);
                        fm.beginTransaction().replace(R.id.main_layout, fragmentoNuevo).addToBackStack(null).commit();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        });
        queue.add(stringRequest);

    }

    public void getCarsByModel(FragmentManager fragmentManager, final String model) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url + "/carsByModel" ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Gson gson = new Gson();
                        Bundle bundle = new Bundle();
                        bundle.putString("cars", response);
                        Fragment fragmentoNuevo = new listDisplay();
                        fragmentoNuevo.setArguments(bundle);
                        fm.beginTransaction().replace(R.id.main_layout, fragmentoNuevo).addToBackStack(null).commit();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("carModel", model);
                return  params;
            }
        };
        queue.add(stringRequest);

    }
    public void getCarsByBrand(FragmentManager fragmentManager, final String model) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url + "/car/" + model ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Gson gson = new Gson();
                        Bundle bundle = new Bundle();
                        bundle.putString("cars", response);
                        Fragment fragmentoNuevo = new listDisplay();
                        fragmentoNuevo.setArguments(bundle);
                        fm.beginTransaction().replace(R.id.main_layout, fragmentoNuevo).addToBackStack(null).commit();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        });

        queue.add(stringRequest);

    }

    public void getCarsByPrice(FragmentManager fragmentManager, final String topPrice, final String botPrice) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url + "/carsByPriceRange" ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Gson gson = new Gson();
                        Bundle bundle = new Bundle();
                        bundle.putString("cars", response);
                        Fragment fragmentoNuevo = new listDisplay();
                        fragmentoNuevo.setArguments(bundle);
                        fm.beginTransaction().replace(R.id.main_layout, fragmentoNuevo).addToBackStack(null).commit();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("topPrice", topPrice);
                params.put("botPrice", botPrice);
                return  params;
            }
        };
        queue.add(stringRequest);

    }

    public void createCar (final Activity entrada, final FragmentManager fragmentManager, final String licensePlate, final int capacity, final int brandId, final int styleId, final String carModel, final String carColor, final int cylinder, final String fuelType, final String transmitionType, final int year, final String extras, final int passengerCapacity, final int dailyCost, final String carStatus) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url + "/cars" ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "car created",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("licensePlate", licensePlate);
                params.put("capacity", Integer.toString(capacity));
                params.put("brandId",Integer.toString(brandId));
                params.put("styleId", Integer.toString(styleId));
                params.put("carModel",carModel);
                params.put("carColor", carColor);
                params.put("cylinder", Integer.toString(cylinder));
                params.put("fuelType", fuelType);
                params.put("transmitionType", transmitionType);
                params.put("year", Integer.toString(year));
                //params.put("extras", extras);
                params.put("passengerCapacity", Integer.toString(passengerCapacity));
                params.put("dailyCost", Integer.toString(dailyCost));
                params.put("carStatus", carStatus);

                return  params;
            }
        };
        queue.add(stringRequest);

    }

    public void updateCar (final Activity entrada, final FragmentManager fragmentManager, final String licensePlate, final int capacity, final int brandId, final int styleId, final String carModel, final String carColor, final int cylinder, final String fuelType, final String transmitionType, final int year, final String extras, final int passengerCapacity, final int dailyCost, final String carStatus) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url + "/cars/" + licensePlate ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "car updated",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("licensePlate", licensePlate);
                params.put("capacity", Integer.toString(capacity));
                params.put("brandId",Integer.toString(brandId));
                params.put("styleId", Integer.toString(styleId));
                params.put("carModel",carModel);
                params.put("carColor", carColor);
                params.put("cylinder", Integer.toString(cylinder));
                params.put("fuelType", fuelType);
                params.put("transmitionType", transmitionType);
                params.put("year", Integer.toString(year));
                //params.put("extras", extras);
                params.put("passengerCapacity", Integer.toString(passengerCapacity));
                params.put("dailyCost", Integer.toString(dailyCost));
                params.put("carStatus", carStatus);

                return  params;
            }
        };
        queue.add(stringRequest);

    }
    public void deleteCar(final Activity entrada, FragmentManager fragmentManager, String plate) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url + "/cars/" + plate ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "car deleted",
                                Toast.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        });
        queue.add(stringRequest);

    }

    public void createClient (final Activity entrada, final FragmentManager fragmentManager, final String clientId, final String fullName, final String email, final String phoneNumber) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url + "/clients" ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "client created",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientId", clientId);
                params.put("fullName", fullName);
                params.put("email",email);
                params.put("phoneNumber", phoneNumber);


                return  params;
            }
        };
        queue.add(stringRequest);

    }

    public void updateClient (final Activity entrada, final FragmentManager fragmentManager, final String clientId, final String fullName, final String email, final String phoneNumber) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url + "/clients/" +clientId ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "client updated",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientId", clientId);
                params.put("fullName", fullName);
                params.put("email",email);
                params.put("phoneNumber", phoneNumber);


                return  params;
            }
        };
        queue.add(stringRequest);

    }

    public void deleteClient(final Activity entrada, FragmentManager fragmentManager, String plate) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url + "/clients/" + plate ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "client deleted",
                                Toast.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        });
        queue.add(stringRequest);

    }

    //hasta aca

    public void createRental (final Activity entrada, final FragmentManager fragmentManager, final String clientId, final String licensePlate, final String duration) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url + "/rentals" ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "Rental created",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientId", clientId);
                params.put("licensePlate", licensePlate);
                params.put("duration",duration);


                return  params;
            }
        };
        queue.add(stringRequest);

    }

    public void updateRental (final Activity entrada, final FragmentManager fragmentManager, final String clientId, final String licensePlate, final String duration) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url + "/rentals/" +clientId ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "rental updated",
                                Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientId", clientId);
                params.put("licensePlate", licensePlate);
                params.put("duration",duration);



                return  params;
            }
        };
        queue.add(stringRequest);

    }

    public void deleteRental(final Activity entrada, FragmentManager fragmentManager, String plate) {
        final FragmentManager fm = fragmentManager;
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url + "/rentals/" + plate ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {// Display the first 500 characters of the response string.
                        Log.i("LLAMADA API", response);

                        Log.d("CREATION", response);
                        Toast.makeText(entrada, "client deleted",
                                Toast.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("LLAMADA API", error.getMessage());
            }
        });
        queue.add(stringRequest);

    }
}
