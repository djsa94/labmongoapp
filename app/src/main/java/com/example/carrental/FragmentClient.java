package com.example.carrental;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragmentClient extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client, container, false) ;
        Button botonBuscar = (Button) view.findViewById(R.id.btnDeleteClient);
        Button botonCrear = (Button) view.findViewById(R.id.btnCrearCliente);
        Button botonUpdate = (Button) view.findViewById(R.id.btnUpdateClient);
        final TextView txID = (TextView) view.findViewById(R.id.clientCRUDid);
        final TextView txFullname = (TextView) view.findViewById(R.id.clientCRUDfullname);
        final TextView txEmail = (TextView) view.findViewById(R.id.clientCRUDemail);
        final TextView txPhoneNumber = (TextView) view.findViewById(R.id.clientCRUDphoneNumber);



        botonCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.createClient(getActivity(), getFragmentManager(), txID.getText().toString(), txFullname.getText().toString(), txEmail.getText().toString(), txPhoneNumber.getText().toString());
            }

        });

        botonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.updateClient( getActivity(), getFragmentManager(), txID.getText().toString(), txFullname.getText().toString(), txEmail.getText().toString(), txPhoneNumber.getText().toString()

                );
            }

        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpUtils utils = new HttpUtils(getActivity());
                utils.deleteClient( getActivity() ,getFragmentManager(), txID.getText().toString()

                );
            }

        });

        return view;


    }
}
