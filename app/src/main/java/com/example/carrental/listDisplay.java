package com.example.carrental;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class listDisplay extends Fragment {
    ArrayList<String> lista;
    Gson gson = new Gson();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_list_display, container, false);
        ArrayList<carModel> lista;
        Gson gson = new Gson();

        TypeToken<ArrayList<carModel>> token = new TypeToken<ArrayList<carModel>>() {
        };
        lista = gson.fromJson(getArguments().getString("cars"), token.getType());
        String[] listaString = new String[lista.size()];
        for(int i=0; i<lista.size(); i++){
            listaString[i] = (lista.get(i).carString());
        }


        ArrayAdapter adapter = new ArrayAdapter<String>(this.getContext(),
                R.layout.layout_textview, listaString);

        ListView listView = (ListView) view.findViewById(R.id.listText);
        listView.setAdapter(adapter);
        return view;
    }

}
